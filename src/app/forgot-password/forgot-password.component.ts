import { Component, OnInit } from '@angular/core';
import {UserService} from '../core/user.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgForm} from '@angular/forms';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  public correo: string;
  public respuesta: string;
  public errorMessage: string;

  constructor(private userService: UserService) { }
  ngOnInit() {
  }

  recoverPassword2() {
    this.errorMessage = '';
    this.userService.recoverPassword(this.correo).subscribe((res) => {
      this.respuesta = 'success';
    }, (error) => {
      this.respuesta = 'error';
      this.errorMessage = error.message;
    });
  }

}
