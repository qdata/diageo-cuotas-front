import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { PasswordConfirmationComponent } from './password-confirmation.component';



const routes: Routes = [
  { path: '', component: PasswordConfirmationComponent }
];

@NgModule({
  declarations: [PasswordConfirmationComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class PasswordConfirmationModule { }
