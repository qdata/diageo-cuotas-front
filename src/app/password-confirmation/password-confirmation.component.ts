import { Component, OnInit } from '@angular/core';
import {UserService} from '../core/user.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-password-confirmation',
  templateUrl: './password-confirmation.component.html',
  styleUrls: ['./password-confirmation.component.css']
})
export class PasswordConfirmationComponent implements OnInit {

  public password1: string;
  public password2: string;
  public respuesta: string;

  private at: string;
  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private router: Router) {
    this.route.queryParams.subscribe(params => {
      this.at = params.access_token;
    });
  }

  ngOnInit() {
  }

  setPassword() {
    if (this.password1 === this.password2) {
      this.userService.setPassword(this.at, this.password1).subscribe((data) => {
        this.router.navigate(['/login'], { queryParams: { changepwd: 1 }, replaceUrl: true });
      });
    } else {
      this.respuesta = 'missmatch';
    }

    // console.log(this.at);
    // this.userService.setPassword(this.at, 'tutunadiecomotutu').subscribe((data) => {
    //   this.router.navigate(['/login'], { queryParams: { changepwd: 1 }, replaceUrl: true });
    // });
  }

}
