import { Injectable, Inject, EventEmitter } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { User, SDKToken } from '../shared/sdk/models';
import { UserApi, LoopBackAuth } from '../shared/sdk/services';

@Injectable()
export class UserService {
  private _user: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  private _loginRequired: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private userApi: UserApi, @Inject(LoopBackAuth) protected auth: LoopBackAuth) {
    this._restoreUser();
  }

  get user(): Observable<any> {
    return this._user.asObservable();
  }

  get loginRequired(): Observable<any> {
    return this._loginRequired.asObservable();
  }

  public requireLogin(): void {
    this._loginRequired.next(true);
  }

  private _restoreUser(): void {
    const user: any = this.auth.getCurrentUserData();
    this._user.next(user);

    if (!user) {
      return;
    }
  }

  public recoverPassword(email: string): Observable<any> {
    this.auth.clear();
    return this.userApi.resetPassword({email});
  }

  public setPassword(token: string, password: string): Observable<any> {
    this.auth.setToken(new SDKToken({
      id: token
    }));
    return this.userApi.setPassword(password).map((data) => {
      this.auth.clear();
    });
  }

  public signIn(user: User): Observable<User> {
    return this.userApi.login(user).map((token: SDKToken) => {
      this.auth.save();
      const currentUser: any = this.auth.getCurrentUserData();
      this._user.next(currentUser);
      return currentUser;
    });
  }

  public signOut(callback?: any): void {
    try {
      this.userApi.logout().subscribe();
    } catch (e) {}
    this.auth.clear();
    this._user.next(null);
    if (callback) {
      callback(null, true);
    }
  }

  public signUp(user: User, callback?: any): Observable<any> {
    return this.userApi.create(user);
  }
}
