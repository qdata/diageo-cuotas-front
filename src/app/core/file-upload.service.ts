import { Injectable, Inject, EventEmitter } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { LoopBackAuth } from '../shared/sdk';

@Injectable()
export class FileUploadService {
  constructor(private http: HttpClient, private auth: LoopBackAuth) {}

  private endpointPrefix: string;

  setUrl(endpoint: string) {
    this.endpointPrefix = endpoint;
  }

  postFile(id: string, fileToUpload: File): Observable<boolean> {
    const endpoint = `${this.endpointPrefix}/api/Offers/${id}/uploadImage`;
    //const endpoint = `https://stage.maiq.co/api/Offers/${id}/uploadImage`;
    const formData: FormData = new FormData();

    formData.append('file', fileToUpload);
    formData.append('name', fileToUpload.name);
    return (
      this.http
        .post(endpoint, formData, {
          headers: { Authorization: `${this.auth.getToken()}` }
        })
        //.post(endpoint, formData, { headers: {Accept: 'multipart/form-data', Authorization: 'TTinhl8wvoQhA5I0cMrdO74IX3ATf2xT2K0gTO0XAT3VSYZmeaZEgMOaV0bNWOm0'}})
        .map(() => {
          return true;
        })
    );
  }

  postDoc(id: string, doc: string, fileToUpload: File): Observable<boolean> {
    const endpoint = `${this.endpointPrefix}/api/Deals/${id}/upload/${doc}`;
    const formData: FormData = new FormData();

    formData.append('file', fileToUpload);
    formData.append('name', fileToUpload.name);
    return (
      this.http
        .post(endpoint, formData, {
          headers: { Authorization: `${this.auth.getToken()}` }
        })
        //.post(endpoint, formData, { headers: {Accept: 'multipart/form-data', Authorization: 'TTinhl8wvoQhA5I0cMrdO74IX3ATf2xT2K0gTO0XAT3VSYZmeaZEgMOaV0bNWOm0'}})
        .map(() => {
          return true;
        })
    );
  }

  uploadFile(path: string, fileToUpload: File): Promise<any> {
    const endpoint = `${this.endpointPrefix}/${path}`;
    const formData: FormData = new FormData();

    formData.append('file', fileToUpload);
    formData.append('name', fileToUpload.name);
    return new Promise((res, rej) => {
      this.http
        .post(endpoint, formData, {
          headers: { Authorization: `${this.auth.getToken()}` }
        })
        //.post(endpoint, formData, { headers: {Accept: 'multipart/form-data', Authorization: 'TTinhl8wvoQhA5I0cMrdO74IX3ATf2xT2K0gTO0XAT3VSYZmeaZEgMOaV0bNWOm0'}})
        .subscribe(
          () => {
            return res();
          },
          err => {
            return rej(err);
          }
        );
    });
  }
}
