import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';

import { Credentials, CredentialsService } from './credentials.service';

import { User } from '../../shared/sdk/models';
import { UserService } from '../user.service';

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable()
export class AuthenticationService {
  private _user: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private credentialsService: CredentialsService, private userService: UserService) {}
  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  login(context: LoginContext): Observable<User> {
    // Replace by proper authentication call
    /* const data = {
      username: context.username,
      token: '123456'
    };
    this.credentialsService.setCredentials(data, context.remember);
    return of(data); */
    return this.userService.signIn({
      email: context.username,
      password: context.password
    } as User);
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.credentialsService.setCredentials();
    this.userService.signOut();
    return of(true);
  }
}
