import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserApi} from '../shared/sdk/services/custom';
import {User} from '../shared/sdk/models';


@Component({
  selector: 'app-dashboard-principal',
  templateUrl: './dashboard-principal.component.html',
  styleUrls: ['./dashboard-principal.component.css']
})
export class DashboardPrincipalComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService, private router: Router,
              private route: ActivatedRoute, private userApi: UserApi) {
  }

  ngOnInit() {
    this.userApi.find({include: 'roles'}).subscribe((res: User[]) => {
      console.log(res);
    });
  }

  logout() {
    this.authenticationService.logout().subscribe(() => this.router.navigate(['/login'], {replaceUrl: true}));
  }
}
