import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardPrincipalComponent } from './dashboard-principal.component';


const routes: Routes = [
  { path: '', component: DashboardPrincipalComponent }
];

@NgModule({
  declarations: [DashboardPrincipalComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class DashboardPrincipalModule { }
