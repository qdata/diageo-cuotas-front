import { Component } from '@angular/core';
import {LoopBackConfig} from './shared/sdk';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'diageo';

  constructor() {
     LoopBackConfig.setBaseURL('http://localhost:3000');
  }
}
