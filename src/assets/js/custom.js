"use strict";
// Window Load
$(window).on("load",function(){
    if($(".custom-scroll").length){
        $(".custom-scroll").mCustomScrollbar();
    }
    if($(".modal-tab-content .tab-pane").length){
        $(".modal-tab-content .tab-pane").mCustomScrollbar({
            theme: 'dark'
        });
    }
    if($("#admin-modal-2 .balance-bottom").length){
        $("#admin-modal-2 .balance-bottom").mCustomScrollbar({
            theme: 'dark'
        });
    }
    if($(".disti-chart-prog").length){
        $(".disti-chart-prog").mCustomScrollbar({
            theme: 'light',
            axis:"x",
            autoExpandScrollbar:true,
			advanced:{autoExpandHorizontalScroll:true}
        });
    }
    
});

// Document Ready
$(document).ready(function(){
    //$("#forget-modal").modal();
    
    
    if($('.selectpicker').length){
        $('.selectpicker').selectpicker();
    }

    if($('#datetimepicker5').length){
        $('#datetimepicker5').datetimepicker({
            viewMode: 'months',
            format: 'MMMM, YYYY',
            icons: {
                previous: 'icon icon-angle-left',
                next: 'icon icon-angle-right',
            },
        });
    }

    $('.check-yr-list').on('click', function(e) {
        if($(this).parent(".dropdown-menu").hasClass('show')) {
            e.stopPropagation();
        }
    });

    /*$(".month-picker").on('click',function(){
        $(this).removeClass("show");
    });

    $("#datetimepicker5").on('click',function(){
        $(this).closest(".month-picker").addClass("show");
    });*/

    $(".navbar-nav > .dropdown > .nav-link").on('click',function(){
        $(".navbar-nav .nav-item").removeClass("active");
        $(this).parent().toggleClass("show");
        $(this).next(".dropdown-menu").slideToggle();
    });

    if($(window).width()<768){
        $(".navbar-nav > .nav-item").on('click',function(){
            $(".navbar-nav .nav-item").removeClass("current active");
            $(this).addClass("current active");
        });
        $(".navbar-nav > .cuota-link").on('click',function(){
            $("body").removeClass("notific-open");
        });
    }

    // Progress bar 
    $('.dept-chart-prog .progress .progress-bar, .battle-block .progress .progress-bar').css("width",
        function() {
            return $(this).attr("aria-valuenow") + "%";
        }
    )

    $('.disti-prog-block .progress-bar').css("height",
        function() {
            return $(this).attr("aria-valuenow") + "%";
        }
    )

    // Toggle button
    // $(".navbar-toggler").on('click',function(){
    //     $("body").addClass("open-navbar");
    // });

    // navbar-close
    // $(".navbar-close").on('click',function(){
    //     $("body").removeClass("open-navbar");
    // });
    
    $(".navbar-toggler").on('click',function(){
        $(".sign-off").slideToggle();
    });
    // notific-nav
    $(".notific-nav").on('click',function(){
        $("body").addClass("notific-open");
        $("body").removeClass("balance-open");
    });

    // notific nav close
    $(".notifi-close").on('click',function(){
        $("body").removeClass("notific-open");
    });

    // balance-nav
    $(".balance-nav").on('click',function(){
        $("body").addClass("balance-open");
        $("body").removeClass("notific-open");
    });

    // balance nav close
    $(".balance-close").on('click',function(){
        $("body").removeClass("balance-open");
    });

    // user-creat
    $(".user-creat, #user-admin-wrap .media").on('click',function(){
        $("#admin-modal-1").fadeIn().addClass("admin-modal-open");
    });

    $("#input-load .media").on('click',function(){
        $("#admin-modal-2").fadeIn().addClass("admin-modal-open");
    });

    $(".admin-modal-overlay,.clear-btn").on('click',function(){
        $(".admin-modal-outer").fadeOut().removeClass("admin-modal-open");
    });

    DataTb();

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        //table.destroy();
        $(".must-do-sel").addClass("d-none");
        $(".battel-sel").addClass("d-none");
        
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust().draw()
        $(".DTFC_LeftBodyLiner .custom-control-label,.DTFC_LeftHeadWrapper .custom-control-label").on('click',function(){
            $(this).toggleClass("has-checki");
        });
    });

    $('a[href="#marca"]').on('shown.bs.tab', function (e) {
        $(".must-do-sel").removeClass("d-none");
    });
    $('a[href="#pdv"]').on('shown.bs.tab', function (e) {
        $(".battel-sel").removeClass("d-none");
    });
    

    //.DTFC_LeftBodyLiner .custom-control-label
    $(".DTFC_LeftBodyLiner .custom-control-label,.DTFC_LeftHeadWrapper .custom-control-label").on('click',function(){
        $(this).toggleClass("has-checki");
    });

    var CustText = $("#vendor-table_length,#direct-table_length,#super-vendor-table_length,#cuo-table_length,#supvisor-table_length,#marca-table_length, #pdv-table_length, #vendor-2-table_length, #pdv-2-table_length, #must-table_length");
    CustText.after('<div class="table-goto d-flex"><span>Ir a:</span> <input type="text" class="goto-control" value="1"></div>');

    //
    $(".punto-pop").on('click',function(){
        $(this).closest(".tab-pane").find(".table-popup").addClass("ts-open");
    });
    $(".close-ts,.ts-overlay").on('click',function(){
        $(this).closest(".table-popup").removeClass("ts-open");
    });

    $(".tab-search .btn-search").on('click',function(){
        $(this).closest(".tab-search").addClass("search-open");
    });
    $(".close-search").on('click',function(){
        $(this).closest(".tab-search").removeClass("search-open");
    });
    
});


function DataTb(){
    if($('.vendor-table').length){
        // Data table
        var table = $('#direct-table,#supvisor-table,#vendor-table').DataTable( {
            destroy: true,
            scrollY:        "",
            scrollX:        true,
            scrollCollapse: true,
            searching: false,
            columnDefs: [{
                targets: [ 0, 1, 2, 3, 4, 5],
                orderable: false,
            }],
            fixedColumns:   {
                leftColumns: 2
            },
            order: [[ 6, 'asc' ]]
        });

        var table = $('#super-vendor-table,#pdv-2-table').DataTable( {
            destroy: true,
            scrollY:        "",
            scrollX:        true,
            scrollCollapse: true,
            searching: false,
            columnDefs: [{
                targets: [ 0, 1, 2, 3,],
                orderable: false,
            }],
            fixedColumns:   {
                leftColumns: 2
            },
            order: [[ 4, 'asc' ]]
        });

        var table = $('#cuo-table').DataTable( {
            destroy: true,
            scrollY:        "",
            scrollX:        true,
            scrollCollapse: true,
            searching: false,
            columnDefs: [{
                targets: [ 0, 1, 2, 3, 5],
                orderable: false,
            }],
            fixedColumns:   {
                leftColumns: 5
            },
            order: [[ 4, 'asc' ]]
        });

        var table = $('#marca-table').DataTable( {
            destroy: true,
            scrollY:        "",
            scrollX:        true,
            scrollCollapse: true,
            searching: false,
            columnDefs: [{
                targets: [ 0, 1, 2],
                orderable: false,
            }],
            order: [[ 3, 'asc' ]]
        });

        var table = $('#must-table').DataTable( {
            destroy: true,
            scrollY:        "",
            scrollX:        true,
            scrollCollapse: true,
            searching: false,
            columnDefs: [{
                targets: [ 0, 1],
                orderable: false,
            }],
            order: [[ 2, 'asc' ]]
        });

        var table = $('#pdv-table,#vendor-2-table').DataTable( {
            destroy: true,
            scrollY:        "",
            scrollX:        true,
            scrollCollapse: true,
            searching: false,
            columnDefs: [{
                targets: [ 0, 1, 2, 3, 4],
                orderable: false,
            }],
            fixedColumns:   {
                leftColumns: 2
            },
            order: [[ 5, 'asc' ]]
        });
    }
}